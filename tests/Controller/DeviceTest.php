<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\DataFixtures\AppFixtures;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;

class DeviceTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        self::bootKernel();
        $manager = self::$container->get('doctrine.orm.entity_manager');
        // static::createClient()->getContainer()->get('doctrine')->getManager();
        $purger = new ORMPurger($manager);
        // $purger->setPurgeMode(ORMPurger::PURGE_MODE_TRUNCATE);
        $purger->purge();
        $manager->getConnection()->exec("ALTER TABLE device AUTO_INCREMENT = 1;");
        $fixtures = new AppFixtures();
        $fixtures->load($manager);
    }

    public function testBase()
    {
        $this->client->request('GET', '/device');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $data = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(5, $data);
        $this->assertSame('Device 0', $data[0]['label']);
        // $this->assertContains('Hello World', $crawler->filter('h1')->text());
    }

    public function testAddSuccess()
    {
        $this->client->request('POST', '/device', [], [], [], json_encode([
            "label" => "test",
            "ip" => "192.1.1.1",
            "os" => "Android",
            "battery" => 3
        ]));
        $this->assertSame(201, $this->client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\DeviceRepository');
        $this->assertSame(6, $repo->count([]));
    }

    public function testDeviceGetOneSuccess()
    {
        $this->client->request('GET', '/device/5');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }
    public function testDeviceGetOneFail()
    {
        $this->client->request('GET', '/device/9000');

        $this->assertSame(204, $this->client->getResponse()->getStatusCode());
    }

    public function testDeviceDeleteSuccess()
    {
        $repo = self::$container->get('App\Repository\DeviceRepository');
        $this->assertSame(5, $repo->count([]));

        $this->client->request('DELETE', '/device/5');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $this->assertSame(4, $repo->count([]));
    }
    public function testDeviceDeleteFail()
    {
        $this->client->request('DELETE', '/device/9000');
        $this->assertSame(204, $this->client->getResponse()->getStatusCode());
    }

    public function testDeviceUpdateSuccess()
    {
        $this->client->request('PATCH', '/device/3', [], [], [], json_encode([
            "label" => "test-update",
        ]));

        $data = json_decode($this->client->getResponse()->getContent(), true);

        $repo = self::$container->get('App\Repository\DeviceRepository');
        $device = $repo->find(3);


        // $this->assertSame('test-update', $device->getLabel());
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
    }
    public function testDeviceUpdateFail()
    {
        $this->client->request('PATCH', '/device/9000');
        $this->assertSame(204, $this->client->getResponse()->getStatusCode());
    }
}
