<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use App\DataFixtures\AppFixtures;
use Symfony\Component\HttpFoundation\JsonResponse;

class AppliTest extends WebTestCase
{
    /**
     * @var Client
     */
    private $client;

    public function setUp()
    {
        $this->client = static::createClient();
        self::bootKernel();
        $manager = self::$container->get('doctrine.orm.entity_manager');
        $purger = new ORMPurger($manager);
        $purger->purge();
        $manager->getConnection()->exec("ALTER TABLE appli AUTO_INCREMENT = 1;");
        $fixtures = new AppFixtures();
        $fixtures->load($manager);
    }

    public function testBase(){
        $this->client->request('GET', '/appli');
        $this->assertSame(JsonResponse::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $data = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertCount(5, $data);
        $this->assertSame('Appli 0', $data[0]['name']);
    }

    public function testAppliGetOneSuccess(){
        $this->client->request('GET', '/appli/4');
        $this->assertSame(JsonResponse::HTTP_OK, $this->client->getResponse()->getStatusCode());
    }
    
    public function testAppliGetOneFail(){
        $this->client->request('GET', '/appli/9000');
        $this->assertSame(204, $this->client->getResponse()->getStatusCode());
    }

    public function testAppliDeleteSuccess(){
        $repo = self::$container->get('App\Repository\AppliRepository');
        $this->assertSame(5, $repo->count([]));
        $this->client->request('DELETE', '/appli/5');
        $this->assertSame(JsonResponse::HTTP_OK, $this->client->getResponse()->getStatusCode());
        $this->assertSame(4, $repo->count([]));
    }

    public function testAppliDeletefail(){
        $this->client->request('DELETE', '/appli/9000');
        $this->assertSame(204, $this->client->getResponse()->getStatusCode());
 
    }

    public function testAppliUpdateSuccess()
    {
        $this->client->request('PATCH', '/appli/3', [], [], [], json_encode([
            "name" => "test-update"
        ]));
        $this->assertSame(JsonResponse::HTTP_OK, $this->client->getResponse()->getStatusCode());


        $appli = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertSame('test-update', $appli['name']);


        // $repo = self::$container->get('App\Repository\AppliRepository');
        // $appli = $repo->find(3);
        // $this->assertSame('test-update', $appli->getName());
    }
    public function testAppliUpdateFail()
    {
        $this->client->request('PATCH', '/appli/9000');
        $this->assertSame(204, $this->client->getResponse()->getStatusCode());
    }

}
