<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Device;
use App\Entity\Appli;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $this->loadDevices($manager);
        $this->loadApplis($manager);
        $manager->flush();
    }
    private function loadDevices(ObjectManager $manager){
        for ($i=0; $i < 5; $i++) { 
            $device = new Device();
            $device->setBattery(rand(0, 100));
            $device->setIp("192.168.1.".$i);
            $device->setLabel("Device ". $i);
            $device->setOs("Os ". $i);
            $manager->persist($device);
        }
    }
    private function loadApplis(ObjectManager $manager){
        for ($i=0; $i < 5; $i++) { 
            $appli = new Appli();
            $appli->setDownloads(rand(0, 42));
            $appli->setLastUpdate(new \DateTime());
            $appli->setName("Appli ". $i);
            $appli->setSize(rand(0, 10));
            $manager->persist($appli);

        }
    }
}
