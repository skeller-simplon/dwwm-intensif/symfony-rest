<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use JMS\Serializer\SerializerInterface;
use App\Repository\AppliRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use App\Form\AppliType;
use App\Entity\Appli;
use PHPUnit\Util\Json;

/**
 * @Route("/appli", name="appli")
 */
class AppliController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializerInterface)
    {
        $this->serializer = $serializerInterface;
    }

    /**
     * @Route(methods="GET")
     */
    public function getAllAppli(AppliRepository $appliRepository)
    {
        return new JsonResponse(
            $this->serializer->serialize(
                $appliRepository->findAll(),
                'json'
            ),
            JsonResponse::HTTP_OK,
            [],
            true
        );
    }
    /**
     * @Route("/{appli}", methods="GET")
     */
    public function getOneAppli(Appli $appli =null){
        if ($appli) {
            return new JsonResponse(
                $this->serializer->serialize($appli, 'json'),
                JsonResponse::HTTP_OK,
                [],
                true
            );    
        }
        return $this->json('Appli not found', JsonResponse::HTTP_NO_CONTENT);

    }
    /**
     * @Route(methods="POST")
     */
    public function appliEdit(ObjectManager $manager, Request $request)
    {
        $appli = new Appli();
        $form = $this->createForm(AppliType::class, $appli);
        $form->submit(
            json_decode(
                $request->getContent(),
                true
            )
        );
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($appli);
            $manager->flush();
            return new JsonResponse(
                $this->serializer->serialize(
                    $appli,
                    'json'
                ),
                201,
                [],
                true
            );
        }
        return $this->json($form->getErrors(true), 400);
    }

    /**
     * @Route("/{appli}", methods="DELETE")
     */
    public function appliDelete(Appli $appli = null, ObjectManager $manager){
        if ($appli) {
            $manager->remove($appli);
            $manager->flush();
            return $this->json("Deleted with success", JsonResponse::HTTP_OK);
        }
        return $this->json('Appli not found', JsonResponse::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/{appli}", methods="PATCH")
     */
    public function appliUpdate(Appli $appli = null, ObjectManager $objectManager, Request $request){
        if ($appli) {
            $form = $this->createForm(AppliType::class, $appli);
            $form->submit(
                json_decode($request->getContent(), true),
                false
            );
            if ($form->isSubmitted() && $form->isValid()) {
                $objectManager->flush();
                return new JsonResponse(
                    $this->serializer->serialize($appli, 'json'),
                    JsonResponse::HTTP_OK,
                    [],
                    true
                );
            }
        }
        return $this->json('Appli not found', JsonResponse::HTTP_NO_CONTENT);
    }
}
