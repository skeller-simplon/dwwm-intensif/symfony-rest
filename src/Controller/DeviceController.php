<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializerInterface;
use App\Repository\DeviceRepository;
use App\Entity\Device;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use App\Form\DeviceType;

/**
 * @Route("/device", name="device")
 */
class DeviceController extends AbstractController
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(SerializerInterface $serializerInterface)
    {
        $this->serializer = $serializerInterface;
    }
    /**
     * @Route(methods="GET")
     */
    public function device(DeviceRepository $deviceRepository)
    {
        return new JsonResponse(
            $this->serializer->serialize(
                $deviceRepository->findAll(),
                'json'
            ),
            200,
            [],
            true
        );
        // return $this->json(["ga" => "bu"]);
    }
    /**
     * @Route(methods="POST")
     */
    public function deviceEdit(ObjectManager $manager, Request $request)
    {
        $device = new Device();
        $form = $this->createForm(DeviceType::class, $device);
        // Le true ici est par défaut et ne sert qu'à set à null les valeur non précisées
        $form->submit(json_decode($request->getContent(), true));
        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($device);
            $manager->flush();
            return new JsonResponse(
                $this->serializer->serialize($device, 'json'),
                201,
                [],
                true
            );
        }
        return $this->json($form->getErrors(true), 400);
    }
    /**
     * @Route("/{device}", methods="GET")
     */
    public function deviceGetOne(Device $device = null)
    {
        if ($device) {
            return new JsonResponse(
                $this->serializer->serialize($device, 'json'),
                200,
                [],
                true
            );    
        }
        return $this->json('Device not found', 204);
    }

    /**
     * @Route("/{device}", methods="DELETE")
     */
    public function deviceDelete(Device $device = null, ObjectManager $objectManager)
    {
        if ($device) {
            $objectManager->remove($device);
            $objectManager->flush();
            return $this->json("Deleted with success", 200);
        }
        return $this->json('Device not found', 204);
    }
    /**
     * @Route("/{device}", methods="PATCH")
     */
    public function deviceUpdate(Device $device = null, ObjectManager $objectManager, Request $request){
        if ($device) {
            $form = $this->createForm(DeviceType::class, $device);
            // Le true ici est par défaut et ne sert qu'à set à null les valeur non précisées
            $form->submit(json_decode($request->getContent(), true), false);
            if ($form->isSubmitted() && $form->isValid()) {
                $objectManager->flush();
                return new JsonResponse(
                    $this->serializer->serialize($device, 'json'),
                    200,
                    [],
                    true
                );
            }
        }
        return $this->json('Device not found', 204);
    }
}
