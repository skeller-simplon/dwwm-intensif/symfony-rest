<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190412082149 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE device_appli (device_id INT NOT NULL, appli_id INT NOT NULL, INDEX IDX_661D7C1C94A4C7D4 (device_id), INDEX IDX_661D7C1C1DC59C41 (appli_id), PRIMARY KEY(device_id, appli_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE device_appli ADD CONSTRAINT FK_661D7C1C94A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE device_appli ADD CONSTRAINT FK_661D7C1C1DC59C41 FOREIGN KEY (appli_id) REFERENCES appli (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE appli_device');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE appli_device (appli_id INT NOT NULL, device_id INT NOT NULL, INDEX IDX_D8CA763094A4C7D4 (device_id), INDEX IDX_D8CA76301DC59C41 (appli_id), PRIMARY KEY(appli_id, device_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE appli_device ADD CONSTRAINT FK_D8CA76301DC59C41 FOREIGN KEY (appli_id) REFERENCES appli (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE appli_device ADD CONSTRAINT FK_D8CA763094A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE device_appli');
    }
}
