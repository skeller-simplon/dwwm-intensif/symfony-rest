<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 */
class Device
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    // @Assert\Regex("\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b")
    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Ip 
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $os;

    /**
     * @ORM\Column(type="integer")
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\LessThanOrEqual(100)
     */
    private $battery;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Appli", inversedBy="Devices")
     */
    private $applis;

    public function __construct()
    {
        $this->applis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getOs(): ?string
    {
        return $this->os;
    }

    public function setOs(string $os): self
    {
        $this->os = $os;

        return $this;
    }

    public function getBattery(): ?int
    {
        return $this->battery;
    }

    public function setBattery(int $battery): self
    {
        $this->battery = $battery;

        return $this;
    }

    /**
     * @return Collection|Appli[]
     */
    public function getApplis(): Collection
    {
        return $this->applis;
    }

    public function addAppli(Appli $appli): self
    {
        if (!$this->applis->contains($appli)) {
            $this->applis[] = $appli;
            $appli->addDevice($this);
        }

        return $this;
    }

    public function removeAppli(Appli $appli): self
    {
        if ($this->applis->contains($appli)) {
            $this->applis->removeElement($appli);
            $appli->removeDevice($this);
        }

        return $this;
    }
}
